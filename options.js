const input = document.forms[0].smooth;
const storage = browser.storage.local;

const key = 'instant';
const { [key]: instant } = await storage.get(key);
input.checked = !instant;

input.addEventListener('change', async () => {
  await storage.set({ [key]: !input.checked });
});
