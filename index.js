// minimal dimensions of an image for it to be taken into account
const sizeLimit = 200;

document.addEventListener('keypress', keypressHandler, false);

function parsePixels(value) {
  const parsed = parseInt(value);
  return Number.isNaN(parsed) ? 0 : parsed;
}

function getTop(image) {
  const parents = [];
  let parent = image;
  while (parent.parentElement) {
    parent = parent.parentElement
    parents.push(parent);
  }

  const scrollables = parents.filter(({ clientHeight, scrollHeight }) => scrollHeight > clientHeight);
  const paddings = scrollables.map((element) =>
    parsePixels(window.getComputedStyle(element)['scroll-padding-top'])
  )

  const allPaddings = paddings.reduce((total, current) => total + current, 0);
  const margin = parsePixels(window.getComputedStyle(image)['scroll-margin-top']);

  return Math.round(image.getClientRects()[0].top) - allPaddings - margin;
}

async function keypressHandler(event) {
  // ignore keypresses with modifiers
  if (event.ctrlKey || event.shiftKey || event.altKey || event.metaKey) return;

  // ignore keypresses in inputs
  const { target } = event;
  if (/input|select|textarea/i.test(target.tagName) || target.hasAttribute('contenteditable')) return;

  // ignore all keys except for R and F
  const key = event.code;
  if (key !== 'KeyR' && key !== 'KeyF') return;
  const next = key === 'KeyF';

  // find the top offsets of all images larger than the limit
  const images = [...document.images]
    .filter(({ offsetWidth, offsetHeight }) => (offsetWidth > sizeLimit && offsetHeight > sizeLimit))
    .map((image) => ({ image, top: getTop(image) }))

  // sort depending on the direction
  const comparator = next ?
    (a, b) => a.top - b.top :
    (a, b) => b.top - a.top;
  images.sort(comparator);

  // scroll to the first image with top offset below the viewport edge (when looking for next image)
  // or above the viewport edge (when looking for previous image).
  const matcher = next ?
    ({ top }) => top > 0 :
    ({ top }) => top < 0;

  const { instant } = (await browser.storage.local.get('instant'));
  const behavior = instant ? 'instant' : 'smooth';

  images.find(matcher)?.image.scrollIntoView({ behavior, block: 'start' });
}
